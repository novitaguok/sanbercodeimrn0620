/**
 * Nomor 1
 */
function bandingkan(num1, num2) {
  if ((num1 == null && num2 == null) || num1 < 0 || num2 < 0 || num1 == num2) {
    return -1;
  } else if (num1 < num2 || num2 > num1) {
    return Math.max(num1, num2);
  } else {
    return num1;
  }
}
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1
console.log(bandingkan(112, 121)); // 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan('15', '18')); // 18
console.log('**********************************');

/**
 * Nomor 2
 */
function balikString(wd) {
  var str = '';
  for (i = wd.length - 1; i >= 0; i--) {
    str += wd[i];
  }
  return str;
}
console.log(balikString('abcde')); // edcba
console.log(balikString('rusak')); // kasur
console.log(balikString('racecar')); // racecar
console.log(balikString('haji')); // ijah
console.log('**********************************');

/**
 * Nomor 3
 */
function palindrome(str) {
  var len = str.length;

  var flag = 1;
  for (var i = 0; i < len; i++) {
    if (str[i] != str[len - i - 1]) {
      var flag = 0;
      break;
    }
  }

  if (flag == 1) {
    flag = 'true';
  } else {
    flag = 'false';
  }
  return flag;
}
console.log(palindrome('kasur rusak')); // true
console.log(palindrome('haji ijah')); // true
console.log(palindrome('nabasan')); // false
console.log(palindrome('nababan')); // true
console.log(palindrome('jakarta')); // false
