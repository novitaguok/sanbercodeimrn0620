/**
 * Nomor 1
 */
class Animal {
  constructor(name, legs = 4, cold_blooded = false) {
    this._name = name;
    this._legs = legs;
    this._cold_blooded = cold_blooded;
  }
  get name() {
    return this._name;
  }
  set name(x) {
    this._name = x;
  }
  get legs() {
    return this._legs;
  }
  set legs(x) {
    this._legs = x;
  }
  get cold_blooded() {
    return this._cold_blooded;
  }
  set cold_blooded(x) {
    this._cold_blooded = x;
  }
}

var sheep = new Animal('shaun');

console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false
console.log('******************************');

/**
 * Nomor 2
 */
class Ape extends Animal {
  constructor(name) {
    super(name, 2, false);
  }
  yell() {
    console.log('Auooo');
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name, 4, true);
  }
  jump() {
    console.log('hop hop');
  }
}
var sungokong = new Ape('kera sakti');
sungokong.yell(); // "Auooo"
console.log(sungokong.legs, sungokong.name, sungokong.cold_blooded);

var kodok = new Frog('buduk');
kodok.jump(); // "hop hop"
console.log(kodok.legs, kodok.name, kodok.cold_blooded);
console.log('******************************');

/**
 * Nomor 3
 */
class Clock {
  constructor({ template }) {
    this.template = template;
  }
  render() {
    this.date = new Date();
    this.hours = this.date.getHours();
    this.mins = this.date.getMinutes();
    this.secs = this.date.getSeconds();
    if (this.hours < 10) this.hours = '0' + this.hours;
    if (this.mins < 10) this.mins = '0' + this.mins;
    if (this.secs < 10) this.secs = '0' + this.secs;
    this.output = this.template
      .replace('h', this.hours)
      .replace('m', this.mins)
      .replace('s', this.secs);

    console.log(this.output);
  }
  stop() {
    clearInterval(this.timer);
  }
  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start();
