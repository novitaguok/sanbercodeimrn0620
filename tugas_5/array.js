/**
 * Nomor 1
 */
function range(startNum, finishNum) {
  if (finishNum == null) {
    return -1;
  } else {
    if (startNum > finishNum) {
      var nums = [];
      for (var i = startNum; i >= finishNum; i--) {
        nums.push(i);
      }
    } else {
      var nums = [];
      for (var i = startNum; i <= finishNum; i++) {
        nums.push(i);
      }
    }

    return nums;
  }
}
console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1
console.log('*******************************');

/**
 * Nomor 2
 */
function rangeWithStep(startNum, finishNum, step) {
  if (finishNum == null) {
    return -1;
  } else {
    if (startNum > finishNum) {
      var nums = [];
      for (var i = startNum; i >= finishNum; i -= step) {
        nums.push(i);
      }
    } else {
      var nums = [];
      for (var i = startNum; i <= finishNum; i += step) {
        nums.push(i);
      }
    }

    return nums;
  }
}
console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]
console.log('*******************************');

/**
 * Nomor 3
 */
function sum(startNum, finishNum, step) {
  if (startNum == null && finishNum == null && step == null) {
    return 0;
  } else if (finishNum == null && step == null) {
    return startNum;
  } else {
    if (step == null) {
      step = 1;
    }

    if (startNum > finishNum) {
      var nums = [];
      for (var i = startNum; i >= finishNum; i -= step) {
        nums.push(i);
      }
    } else {
      var nums = [];
      for (var i = startNum; i <= finishNum; i += step) {
        nums.push(i);
      }
    }
    sum_all = nums.reduce((a, b) => a + b, 0);
    return sum_all;
  }
}
console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0
console.log('*******************************');
/**
 * Nomor 4
 */
function dataHandling(input, n) {
  for (var i = 0; i < n; i++) {
    console.log(
      'Nomor ID: ' +
        input[i][0] +
        '\n' +
        'Nama Lengkap: ' +
        input[i][1] +
        '\n' +
        'TTL: ' +
        input[i][2] +
        input[i][3] +
        '\n' +
        'Hobi: ' +
        input[i][4] +
        '\n' +
        '\n'
    );
  }
}
var input = [
  ['0001', 'Roman Alamsyah', 'Bandar Lampung', '21/05/1989', 'Membaca'],
  ['0002', 'Dika Sembiring', 'Medan', '10/10/1992', 'Bermain Gitar'],
  ['0003', 'Winona', 'Ambon', '25/12/1965', 'Memasak'],
  ['0004', 'Bintang Senjaya', 'Martapura', '6/4/1970', 'Berkebun'],
];
dataHandling(input, input.length);
console.log('*******************************');

/**
 * Nomor 5
 */
function balikKata(wd) {
  var str = '';
  for (i = wd.length - 1; i >= 0; i--) {
    str += wd[i];
  }
  return str;
}

console.log(balikKata('Kasur Rusak')); // kasuR rusaK
console.log(balikKata('SanberCode')); // edoCrebnaS
console.log(balikKata('Haji Ijah')); // hajI ijaH
console.log(balikKata('racecar')); // racecar
console.log(balikKata('I am Sanbers')); // srebnaS ma I
console.log('*******************************');

/**
 * Nomor 6
 */
function dataHandling2(input) {
  /**
   * Subproblem 1
   */
  // Splice name
  var nm = input[1];
  var nm_split = nm.split(' ');
  nm_split.splice(2, 1, 'Elsharawy');
  var nm_final = nm_split.join(' ');
  input.splice(1, 1, nm_final);

  // Splice province
  var prov = input[2];
  var prov_split = prov.split(' ');
  prov_split.splice(0, 0, 'Provinsi');
  var prov_final = prov_split.join(' ');
  input.splice(2, 1, prov_final);

  // Splice other
  input.splice(4, 0, 'Pria');
  input.splice(5, 1, 'SMA International Metro');

  // Output
  console.log(input);
  console.log('..........................................');

  /**
   * Subproblem 2
   */
  var date = input[3];
  var date_split = date.split('/');
  var month = date_split[1];

  switch (month) {
    case '01':
      month = 'Januari';
      break;
    case '02':
      month = 'Febuari';
      break;
    case '03':
      month = 'Maret';
      break;
    case '04':
      month = 'April';
      break;
    case '05':
      month = 'Mei';
      break;
    case '06':
      month = 'Juni';
      break;
    case '07':
      month = 'Juli';
      break;
    case '08':
      month = 'Agustus';
      break;
    case '09':
      month = 'September';
      break;
    case '10':
      month = 'Oktober';
      break;
    case '11':
      month = 'November';
      break;
    case '12':
      month = 'Desember';
      break;
  }
  console.log(month);
  console.log('..........................................');

  /**
   * Subproblem 3
   */
  function sorted(date) {
    console.log(
      date.concat().sort(function (a, b) {
        return b.length - a.length;
      })
    );
  }
  date_sort = sorted(date_split);
  console.log('..........................................');

  /**
   * Subproblem 4
   */
  console.log(date_split.join('-'));
  console.log('..........................................');

  /**
   * Subproblem 5
   */
  nm_limited = nm_final.slice(0, 14);
  console.log(nm_limited);
}

// Main
var input = [
  '0001',
  'Roman Alamsyah ',
  'Bandar Lampung',
  '21/05/1989',
  'Membaca',
];

dataHandling2(input);
