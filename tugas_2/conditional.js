/**
 * Nomor 1
 */
var nama = 'John'
var peran = ''

if (nama == '' && peran == '') {
  console.log('Nama harus diisi!')
} else if (peran == '') {
  console.log('Hello ' + nama + ', pilih peranmu untuk memulai game!')
} else {
  console.log('Selamat datang di Dunia Werewolf, ' + nama)
  if (peran == 'Penyihir' || peran == 'penyihir') {
    console.log(
      'Hello ' +
        peran +
        ' ' +
        nama +
        ', kamu dapat melihat siapa yang menjadi Werewolf.',
    )
  } else if (peran == 'Guard' || peran == 'guard') {
    console.log(
      'Hello ' +
        peran +
        ' ' +
        nama +
        ', kamu akan membantu melindungi temanmu dari serangan Werewolf',
    )
  } else if (peran == 'Werewolf' || peran == 'werewolf') {
    console.log(
      'Hello ' + peran + ' ' + nama + ', kamu kan memakan mangsa setiap malam!',
    )
  }
}
console.log('*********************************************')

/**
 * Nomor 2
 */
var hari = 21
var bulan = 1
var tahun = 1945

switch (bulan) {
  case 1:
    bulan = 'Januari'
  case 2:
    bulan = 'Febuari'
  case 3:
    bulan = 'Maret'
  case 4:
    bulan = 'April'
  case 5:
    bulan = 'Mei'
  case 6:
    bulan = 'Juni'
  case 7:
    bulan = 'Juli'
  case 8:
    bulan = 'Agustus'
  case 9:
    bulan = 'September'
  case 10:
    bulan = 'Oktober'
  case 11:
    bulan = 'November'
  case 12:
    bulan = 'Desember'
}
console.log(hari + ' ' + bulan + ' ' + tahun)
